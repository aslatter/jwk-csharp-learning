﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using Microsoft.IdentityModel.Tokens;

// path to pfx containing signing key
var pfxPath = args[0];

// path to jwks to verify with
var jwksPath = args[1];

//
// issue token
//

var cert = new X509Certificate2(pfxPath);
var signingCreds = signingCredsFromPfx(cert);

var ident = new ClaimsIdentity(new Claim[]{
    new Claim("sub", "some subject"),
});

var d = new SecurityTokenDescriptor{
    Audience = "https://audience/",
    Issuer = "https://issuer/",
    Subject = ident,
    Expires = DateTime.UtcNow.AddDays(1),
    Claims = new Dictionary<string, object>{
        ["foo"] = "bar",
        ["complex"] = new Dictionary<string,object>{
            ["key"] = "value",
        },
    },
    SigningCredentials = signingCreds,
};

var h = new JwtSecurityTokenHandler();
var tk = h.CreateJwtSecurityToken(d);

var tkStr = h.WriteToken(tk);

Console.Out.WriteLine("token: {0}", tkStr);

//
// verify token
//

// jwks created with 'genjwks' utility
var jwks = new JsonWebKeySet(File.ReadAllText(jwksPath));

var principal = h.ValidateToken(tkStr, new TokenValidationParameters{
    IssuerSigningKeys = jwks.Keys,
    ValidAudiences = new string[]{"https://audience/"},
    ValidIssuers = new string[]{"https://issuer/"},
    ValidAlgorithms = new string[]{"ES256", "RS256"},
}, out var validToken);

Console.Out.WriteLine("Validated token:{0}", validToken);

foreach (var c in principal.Claims) {
    Console.Out.WriteLine($"{c.Type} = {c.Value}");
}

SigningCredentials signingCredsFromPfx(X509Certificate2 cert) {
    var rsaKey = cert.GetRSAPrivateKey();
    if (rsaKey != null) {
        // assume we want RSA256 if we have an RSA cert? hacky.
        return new SigningCredentials(new RsaSecurityKey(rsaKey), "RS256");
    }
    var ecdsaKey = cert.GetECDsaPrivateKey();
    if (ecdsaKey != null) {
        var algorithm = $"ES{ecdsaKey.KeySize}";
        return new SigningCredentials(new ECDsaSecurityKey(ecdsaKey), algorithm);
    }

    // .NET does not natively support Ed25519/EdDSA

    throw new ArgumentException("unable to determine signing credential for key");
}