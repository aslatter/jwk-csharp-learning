
# ECDSA P-256

```sh
# generate private key
openssl ecparam -name prime256v1 -genkey -noout -out private.ec.key

# export public key
openssl ec -in private.ec.key -pubout -out public.ec.pem

# generate self-signed-cert (only contains public-key-material)
openssl req -new -x509 -key private.ec.key -out publickey.cer -days 365

# generate pkcs12 cert (contains both public and private key material)
openssl pkcs12 -export -out public_privatekey.pfx -inkey private.ec.key -in publickey.cer
```

# RSA

```sh
# generate a private key
openssl genrsa -out private.rsa.key 4096

# export public key
openssl rsa -in private.rsa.key -pubout -out public.rsa.pem

# generate a self-signed-cert (only contains public key)
openssl req -new -x509 -key private.rsa.key -out publickey.rsa.cer -days 365

# generate a pkcs12 cert (contains public and private key)
openssl pkcs12 -export -out private.rsa.pfx -inkey private.rsa.key -in publickey.rsa.cer
```

# Ed25519

```sh
# generate a private key
openssl genpkey -algorithm ed25519 -out private.ed25519.key

# export public key 
openssl pkey -in private.ed25519.key -pubout -out public.ed25519.pem

# generate a self-signed-cert
openssl req -new -x509 -key private.ed25519.key -out publickey.ed25519.cer -days 365

# generate a pkcs12 cert
openssl pkcs12 -export -out private.ed25519.pfx -inkey private.ed25519.key -in publickey.ed25519.cer

```