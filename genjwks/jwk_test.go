package main

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
)

const testjwks = `
{
	"keys": [
	  {
		"crv": "P-256",
		"kty": "EC",
		"x": "vZVRxCfI9IHdaN-niyQI1LXIIygALC_WKxLiqEgi100",
		"y": "zNSHwQnUWVg2QA5BxNUyPud2Vol2XOEQh9tLppmXRuw"
	  },
	  {
		"e": "AQAB",
		"kty": "RSA",
		"n": "tXA6irSEVqz0v8OygeuwKEEiFG-_VLG1ExFdtbAyycZCKnkICQsGI18SgdEJnrCk8xMSbH94ekHUzurRYrpHQUaYXA5_QMJGZUoKEKKi9RDAeun1bi5vQhbF-HlbJvSNYsf27dmEqTTwKQAK5SuNlEl7EwxV8NVhUwfxmwEJSCwKOAGmbG6olVaJ_PAvtvK755gLWKbYK5PRg6wG5abRwTdzqGT-1Ijv6JYvZFk4B0kzAQ_80lLUs-DfEqHH1_hcjB7zIXiZvpLLFfSDS7idFCX0J7uJla8FDKBpieRrwcrZJe21-PuyeMNDyeSQClUGO4PWLUPqFnuyM3XAvgsdwAyo8nan5LLlYxYCVfinCWMZ5pfbKsY6A5XJcjjlwd3-iYHIH71PC4EDTfuUmpd3nPsMHJzSWA4ACJPxecBUgTHdsBKTlgzulztfWb7GvXS2UJdsPSbGDrdTorqUGz0LvrO4DZaWJZgrhlB3PVedWYbwpN6hhWTvSnF7Xga-mA-otAorcw553Moklrnwpmkk6KTqkkNGqRpDo1cuGdcwJPnJ8XGDK0PtyDTQi_Eh6UzzohxxGdeRZzA5gYAkVZTaVSQQDkVfbr0PdsdTA2ILaJ2RlqWlM1_XAloXPL_OM3FmGFDX_OmxQ3QV6paTAhKbWemoQzByf5nWoN4rNhoWK6s"
	  },
	  {
		"crv": "Ed25519",
		"kty": "OKP",
		"x": "JlzhSsnRoqEC1lZbdgckZcDtR6RtYlAYl0KoD144HHQ"
	  }
	]
  }
`

func TestJson(t *testing.T) {
	t.Run("from string round trip", func(t *testing.T) {
		var ks JsonWebKeySet
		err := json.Unmarshal([]byte(testjwks), &ks)
		if err != nil {
			t.Fatalf("unmarshaling test data: %s", err)
		}
		actualBytes, err := json.Marshal(&ks)
		if err != nil {
			t.Fatalf("marshalling key-set: %s", err)
		}
		assertAreJsonEqual(t, testjwks, string(actualBytes))
	})
}

func assertAreJsonEqual(t *testing.T, expected string, actual string) {
	var e any
	var a any

	err := json.Unmarshal([]byte(expected), &e)
	if err != nil {
		t.Helper()
		t.Fatalf("unable to unmarshal expected value: %s: %q", err, expected)
	}

	err = json.Unmarshal([]byte(actual), &a)
	if err != nil {
		t.Helper()
		t.Fatalf("unable to unmarshal actual value: %s: %q", err, actual)
	}

	diff := cmp.Diff(e, a)
	if diff != "" {
		t.Helper()
		t.Fatal(diff)
	}
}
