package main

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"os"
)

func main() {
	// args are paths to pem-encoded public keys.
	// produces a jwks containing the keys to stdout.

	// no flags
	flag.Parse()

	paths := flag.Args()

	keySet := []JsonWebKey{}

	for _, inFileName := range paths {

		bytes, err := os.ReadFile(inFileName)
		if err != nil {
			panic(err)
		}

		pkBytes, _ := pem.Decode(bytes)

		anyKey, err := x509.ParsePKIXPublicKey(pkBytes.Bytes)
		if err != nil {
			panic(err)
		}

		var result JsonWebKey

		switch k := anyKey.(type) {
		case *rsa.PublicKey:
			result = rsaToJWK(k)
		case *ecdsa.PublicKey:
			result = ecdsaToJWK(k)
		case ed25519.PublicKey:
			result = ed25519ToJwk(k)
		default:
			panic(fmt.Errorf("unknow key type: %T", k))
		}

		keySet = append(keySet, result)
	}

	jwks := JsonWebKeySet{
		Keys: keySet,
	}

	out, err := json.MarshalIndent(jwks, "", "  ")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(out))
}

func rsaToJWK(k *rsa.PublicKey) JsonWebKey {
	e := JWKUInt(k.E)
	return JsonWebKey{
		KeyType: "RSA",
		N:       k.N.Bytes(),
		E:       &e,
	}
}

func ecdsaToJWK(k *ecdsa.PublicKey) JsonWebKey {
	return JsonWebKey{
		KeyType: "EC",
		Curve:   k.Curve.Params().Name,
		X:       k.X.Bytes(),
		Y:       k.Y.Bytes(),
	}
}

func ed25519ToJwk(k ed25519.PublicKey) JsonWebKey {
	return JsonWebKey{
		KeyType: "OKP",
		Curve:   "Ed25519",
		X:       JWKBytes(k),
	}
}

type JsonWebKeySet struct {
	Keys []JsonWebKey `json:"keys"`
}

type JsonWebKey struct {
	KeyType string   `json:"kty,omitempty"`
	Curve   string   `json:"crv,omitempty"`
	X       JWKBytes `json:"x,omitempty"`
	Y       JWKBytes `json:"y,omitempty"`
	N       JWKBytes `json:"n,omitempty"`
	E       *JWKUInt `json:"e,omitempty"`

	// a jwk may also contain private-key-material, but that's
	// unimplemeted here.

	// a jwk may also contain a kery-id, x509 cert, an
	// x509 cert thumbprint, and other fields.
}

// JWKBytes marhsals as URL Base64 (with no padding)
type JWKBytes []byte

func (i JWKBytes) MarshalText() (text []byte, err error) {
	return []byte(base64.RawURLEncoding.EncodeToString(i)), nil
}
func (i *JWKBytes) UnmarshalText(text []byte) error {
	b := make([]byte, base64.RawURLEncoding.DecodedLen(len(text)))
	l, err := base64.RawURLEncoding.Decode(b, text)
	if err != nil {
		return err
	}
	*i = b[:l]
	return nil
}

// JKUInt implements Base64UrlUInt from RFC 7515
type JWKUInt uint64

func (i JWKUInt) MarshalText() (text []byte, err error) {
	b := binary.BigEndian.AppendUint64(make([]byte, 0, 8), uint64(i))
	for len(b) > 1 && b[0] == 0 {
		b = b[1:]
	}
	str := base64.RawURLEncoding.EncodeToString(b)
	return []byte(str), nil
}

func (i *JWKUInt) UnmarshalText(text []byte) error {
	b := make([]byte, 8)
	l, err := base64.RawURLEncoding.Decode(b, text)
	if err != nil {
		return err
	}
	if l < 8 {
		b2 := b
		b = make([]byte, 8)
		copy(b[8-l:], b2)
	}
	n := binary.BigEndian.Uint64(b)
	*i = JWKUInt(n)
	return nil
}
